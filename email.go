package email

import (
	"encoding/json"
	"net/mail"
)

type (
	ToAddress     string
	FromAddress   string
	EmailBody     string
	RecipientName string
)

func (a *ToAddress) UnmarshalJSON(input []byte) error {
	if m, err := unmarshalEmail(input); err != nil {
		return ErrToAddressDidntParse{err}
	} else {
		*a = ToAddress(m)
		return nil
	}
}

func (a *FromAddress) UnmarshalJSON(input []byte) error {
	if m, err := unmarshalEmail(input); err != nil {
		return ErrFromAddressDidntParse{err}
	} else {
		*a = FromAddress(m)
		return nil
	}
}

func unmarshalEmail(input []byte) (string, error) {
	var s string
	if err := json.Unmarshal(input, &s); err != nil {
		return "", err
	} else if err := validateAddress(s); err != nil {
		return "", err
	}
	return s, nil
}

type email struct {
	To        ToAddress     `json:"to"`
	From      FromAddress   `json:"from"`
	Body      EmailBody     `json:"body"`
	Recipient RecipientName `json:"name"`
}

func (e email) ToAddress() ToAddress         { return e.To }
func (e email) FromAddress() FromAddress     { return e.From }
func (e email) EmailBody() EmailBody         { return e.Body }
func (e email) RecipientName() RecipientName { return e.Recipient }

type Email interface {
	ToAddress() ToAddress
	FromAddress() FromAddress
	EmailBody() EmailBody
	RecipientName() RecipientName
}

type (
	ErrToAddressDidntParse   struct{ reason error }
	ErrFromAddressDidntParse struct{ reason error }
)

func (e ErrToAddressDidntParse) Error() string {
	return "'To' address didn't parse: " + e.reason.Error()
}

func (e ErrFromAddressDidntParse) Error() string {
	return "'From' address didn't parse: " + e.reason.Error()
}

func NewEmail(to ToAddress, from FromAddress, body EmailBody, name RecipientName) (Email, []error) {
	errors := []error{}
	if err := validateAddress(string(to)); err != nil {
		errors = append(errors, ErrToAddressDidntParse{err})
	}
	if err := validateAddress(string(from)); err != nil {
		errors = append(errors, ErrFromAddressDidntParse{err})
	}
	if len(errors) > 0 {
		return nil, errors
	}
	return &email{To: to, From: from, Body: body, Recipient: name}, errors
}

func validateAddress(address string) error {
	_, err := mail.ParseAddress(address)
	return err
}

func FromJSON(input []byte) (Email, error) {
	var e email
	if err := json.Unmarshal(input, &e); err != nil {
		return nil, err
	}
	return &e, nil
}
