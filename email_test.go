package email

import (
	"log"
	"reflect"
	"testing"
)

var (
	goodJson = `{   "to": "levi@startup.com",
	   "from": "chris@website.org",
	   "body": "hello!",
	   "name": "Levi"}`
	goodEmail = MustNewEmail(ToAddress("levi@startup.com"),
		FromAddress("chris@website.org"),
		EmailBody("hello!"),
		RecipientName("Levi"))
	jsonMissingKey = `{ "to": "levi@startup.com",
		"from": "chris@website.org",
		"body": "hello!"}`
	jsonBadEmail = `{ "to": "levi@startup.com",
		"from": "chrisLOL",
		"body": "hello!",
		"name": "Levi"}`
	to   = ToAddress("levi@startup.com")
	from = FromAddress("chris@website.org")
	body = EmailBody("hi!")
	name = RecipientName("Levi")
)

func MustNewEmail(to ToAddress, from FromAddress, body EmailBody, name RecipientName) Email {
	if e, err := NewEmail(to, from, body, name); len(err) > 0 {
		panic(err)
	} else {
		return e
	}
}

func TestInitialization(t *testing.T) {
	/*
		_ := email{
			ToAddress:     from,
			FromAddress:   to,
			EmailBody:     body,
			RecipientName: name,
		}

		Error:
		./email_test.go:12: cannot use from (type FromAddress) as type ToAddress in field value
		./email_test.go:13: cannot use to (type ToAddress) as type FromAddress in field value
	*/

	_ = email{
		To:        to,
		From:      from,
		Body:      body,
		Recipient: name,
	}
}

func TestSmartConstructor(t *testing.T) {
	if _, err := NewEmail(ToAddress("PLAID"), FromAddress("TROLOLOL"), body, name); len(err) == 0 {
		t.Fatal("Malformed emails in 'To' and 'From' fields not detected")
	} else {
		log.Println(err) // repl ;)
	}
	if _, err := NewEmail(to, from, body, name); len(err) > 0 {
		t.Fatalf("Correct invocation failed: '%v'", err)
	}
}

func TestGoodJson(t *testing.T) {
	if e, err := FromJSON([]byte(goodJson)); err != nil {
		t.Fatal(err)
	} else if !reflect.DeepEqual(goodEmail, e) {
		t.Fatalf("Expecting '%#v', got '%#v'", goodEmail, e)
	}
}

func TestJsonMissingKey(t *testing.T) {
	if _, err := FromJSON([]byte(jsonMissingKey)); err != nil {
		t.Fatal("Parsing of json wiht missing key is fine for encoding/json")
	}
}

func TestJsonBadEmail(t *testing.T) {
	_, err := FromJSON([]byte(jsonBadEmail))
	if err == nil {
		t.Fatal("Parsing of json with invalid emails should fail")
	}
	log.Println(err) // repl ;)
}
